from django.http.response import HttpResponse
from django.shortcuts import redirect, render
from.models import student
from.models import teachers
from.models import category
from.models import product
from django.core.files.storage import FileSystemStorage
from django.contrib.auth import logout



def addstudent(request):
    if request.method=='POST':
        f=request.POST.get('first_name')
        l=request.POST.get('last_name')
        b=request.POST.get('birthday')
        g=request.POST.get('gender')
        e=request.POST.get('email')
        ph=request.POST.get('phone')
        s=request.POST.get('subject')
        us=request.POST.get('uname')
        psw=request.POST.get('pssword')
        student.objects.create(fname=f,lname=l,dob=b,gen=g,email=e,mob=ph,sub=s,u=us,psswd=psw)
        return render(request,"login.html")
    return render(request,"index.html")
def getuser(request):
    k=teachers.objects.all()
    if request.method=='POST':
        se=request.POST.get('name')
        res=teachers.objects.filter(fn=se)
        return render(request,"table.html",{'data':res})
    
    return render(request,"table.html",{"data":k})
 

def markfilter(request):
    m=teachers.objects.all()
    if request.method=="POST":
        m1=request.POST.get('mark1')
        m2=request.POST.get('mark2')
        d=teachers.objects.filter(m1__range=(m1,m2))
        return render(request, "table.html",{'data':d})
    return render(request,"table.html",{'data':m})

def deluser(request,userid):
    x=teachers.objects.get(id=userid)
    x.delete()
    return redirect("all")

def update_user(request,userid):
    x=teachers.objects.filter(id=userid).values()
    if request.method=="POST":
        F=request.POST.get('f_name')
        L=request.POST.get('l_name')
        G=request.POST.get('gen')
        B=request.POST.get('dob')
        M=request.POST.get('num')
        E=request.POST.get('email')
        M1=request.POST.get('mark1')  
        M2=request.POST.get('mark2')      
        x.update(fn=F,ln=L,ge=G,pho=M,d=B,ema=E,m1=M1,m2=M2)
        return redirect("all")
    return render(request,"update.html",{"userdata":x[0],"id":userid})
    
def login_user(request):
    if request.method=="POST":
        us=request.POST.get('username')
        pw=request.POST.get('password')
        if student.objects.filter(u=us,psswd=pw).exists():
            k=student.objects.filter(u=us,psswd=pw).values().first()
            request.session['user_email']=k.get('u')
            return redirect("all")
        else:
           return HttpResponse("Invalid User!!")
    return render(request,"login.html")
def new(request):
    k=teachers.objects.all()
    return render(request,"intro.html",{"data":k})

def teach(request):
    if request.method=='POST':
        fa=request.POST.get('fname')
        la=request.POST.get('lname')
        da=request.POST.get('birth')
        gend=request.POST.get('gen')
        ea=request.POST.get('email')
        pa=request.POST.get('pho')
        im=request.FILES['img']
        fo=FileSystemStorage()
        im2=fo.save(im.name,im)
        bran=request.POST.get('branch')
        ma=request.POST.get('mark1')
        m2a=request.POST.get('mark2')
        teachers.objects.create(fn=fa,ln=la,d=da,ge=gend,ema=ea,pho=pa,br=bran,m1=ma,m2=m2a,profile_pic=im2)
        return redirect("all")
    return render(request,"stud.html")

def logout_user(request):
    logout(request)
    return redirect('login')

          
def addproduct(request):
    c=category.objects.create(cname="watches")
    product.objects.create(pname="fastrack",price=2000,cat=c)
    return HttpResponse("product added successfully!!")

    
    
   

