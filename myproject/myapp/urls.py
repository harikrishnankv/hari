from django.urls import path
from .views import *
from MyApp import views

urlpatterns = [
    path('',views.new,name="h"),
    path('home',addstudent,name="add"),
    path('getall',views.getuser,name="all"),
    path('mfilter',views.markfilter,name="mark"),
    path('del<int:userid>',views.deluser,name="dl"),
    path('edit<int:userid>',views.update_user,name="ed"),
    path('log',views.login_user,name="login"),
    path('get',views.teach,name="st"),
    path('logout',views.logout_user,name="logout"),
    
]
