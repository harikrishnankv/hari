# Generated by Django 4.0 on 2021-12-30 05:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('MyApp', '0002_alter_student_gen'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='gen',
            field=models.CharField(choices=[('Male', 'Male'), ('Female', 'Female')], max_length=30, null=True),
        ),
    ]
